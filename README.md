## Python script for MQ monitoring

execute **python monitor.py** to start monitoring MQManager

0. PyMqi library should be installed
1. Before running script export MQSERVER variable.
For example in mq_properties.json.origin it is __export MQSERVER="GWAYM3.MONITOR/TCP/10.121.32.168(1417)"__



|File|Description|
|---|---|
|conn.py|Reads MQ properties from resource/mq_properties.json file|
|mq_message.py|Contains classes for MQMessage structuring and parsing|
|mq_properties.json|File with MQ value|
|mq_properties.json.origin|Example file with MQ values|


##### Misc
[PyMQI documentation](https://pythonhosted.org/pymqi/)

@pliskin-effem Alexander Pliskin (BAI)