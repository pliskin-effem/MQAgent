import binascii
import json
import pymqi

parameter_names = {
    pymqi.CMQC.MQIA_MSG_ENQ_COUNT: 'Msg enq count',
    pymqi.CMQC.MQIA_MSG_DEQ_COUNT: 'Msg deq count',
    pymqi.CMQC.MQIA_HIGH_Q_DEPTH: 'High queue depth',
    pymqi.CMQC.MQIA_TIME_SINCE_RESET: 'Time since reset',
    pymqi.CMQC.MQCA_Q_MGR_NAME: 'QManager name',
    pymqi.CMQCFC.MQCACH_CHANNEL_NAME: 'Channel name',
    pymqi.CMQCFC.MQCACH_XMIT_Q_NAME: 'Xmit queue name',
    pymqi.CMQCFC.MQCACH_CONNECTION_NAME: 'Connection name',
    pymqi.CMQCFC.MQIACF_REASON_QUALIFIER: 'Reason qualifier',
    pymqi.CMQCFC.MQIACF_AUX_ERROR_DATA_INT_1: 'Aux error data INT 1',
    pymqi.CMQCFC.MQIACF_AUX_ERROR_DATA_INT_2: 'Aux error data INT 2',
    pymqi.CMQCFC.MQCACF_AUX_ERROR_DATA_STR_1: 'Aux error data STR 1',
    pymqi.CMQCFC.MQCACF_AUX_ERROR_DATA_STR_2: 'Aux error data STR 2',
    pymqi.CMQCFC.MQCACF_AUX_ERROR_DATA_STR_3: 'Aux error data STR 3',
    pymqi.CMQCFC.MQIACF_ERROR_ID: 'Error id',
    pymqi.CMQC.MQRC_ALREADY_CONNECTED: 'Already connected'
}

parameter_types = {
    pymqi.CMQCFC.MQCFT_INTEGER: 'mq_integer',
    pymqi.CMQCFC.MQCFT_STRING: 'mq_string',
    pymqi.CMQCFC.MQCFT_INTEGER_LIST: 'mq_integer_list',
    pymqi.CMQCFC.MQCFT_STRING_LIST: 'mq_string_list',
    pymqi.CMQCFC.MQCFT_BYTE_STRING: 'mq_string_byte',
    pymqi.CMQCFC.MQCFT_GROUP: 'mq_group'
}

BYTE_SIZE = 4
PARAMS_START_INDEX = 36


def byte2int(arr, start, end):
    val = arr[start:end]
    val.reverse()
    return int(binascii.hexlify(val), 16)


def byte2str(arr, start, end):
    return str(arr[start:end]).strip()


class MQMessage:
    """
        Class to describe and collect message attributes
    """

    def __init__(self, message_type, struct_length, version, command,
                 seq_number, control, comp_code, reason_code, params, md):
        self.message_type = message_type
        self.struct_length = struct_length
        self.version = version
        self.command = command
        self.seq_number = seq_number
        self.control = control
        self.comp_code = comp_code
        self.reason_code = reason_code
        self.params = params
        self.md = md

    def to_json(self):
        message_info = {
            'reason_code': self.reason_code,
            'message_type': self.message_type,
            'params': self.params
        }
        return json.dumps(message_info, indent=4, ensure_ascii=False)


class MQMessageParser:
    """
    Class to parse message attributes
    """

    def __init__(self):
        pass

    def parse(self, message, md):
        message = bytearray(message)
        mq_message = MQMessage(self._parse_message_type(message),
                               self._parse_struct_length(message),
                               self._parse_version(message),
                               self._parse_command(message),
                               self._parse_seq_number(message),
                               self._parse_control(message),
                               self._parse_comp_code(message),
                               self._parse_reason_code(message),
                               self._parse_params(message),
                               md)
        return mq_message

    @staticmethod
    def _parse_message_type(message):
        return byte2int(message, 0, 4)

    @staticmethod
    def _parse_struct_length(message):
        return byte2int(message, 4, 8)

    @staticmethod
    def _parse_version(message):
        return byte2int(message, 8, 12)

    @staticmethod
    def _parse_command(message):
        return byte2int(message, 12, 16)

    @staticmethod
    def _parse_message_struct_length(message):
        return byte2int(message, 4, 8)

    @staticmethod
    def _parse_version(message):
        return byte2int(message, 8, 12)

    @staticmethod
    def _parse_command(message):
        return byte2int(message, 12, 16)

    @staticmethod
    def _parse_seq_number(message):
        return byte2int(message, 16, 20)

    @staticmethod
    def _parse_control(message):
        return byte2int(message, 20, 24)

    @staticmethod
    def _parse_comp_code(message):
        return byte2int(message, 24, 28)

    @staticmethod
    def _parse_reason_code(message):
        return byte2int(message, 28, 32)

    @staticmethod
    def _parse_params_count(message):
        return byte2int(message, 32, 36)

    def _parse_params(self, message):
        params = []
        index = PARAMS_START_INDEX
        while index < len(message):
            index = self._parse_param(message, index, params)
        return params

    def _parse_param(self, message, index, params):
        param_type = parameter_types[byte2int(message, index, index + BYTE_SIZE)]
        method_name = '_process_' + param_type + '_param'
        method = getattr(self, method_name)
        return method(message, index, params)

    @staticmethod
    def _process_mq_integer_param(message, index, params):
        struct_length = byte2int(message, index + BYTE_SIZE, index + BYTE_SIZE * 2)
        param_id = byte2int(message, index + BYTE_SIZE * 2, index + BYTE_SIZE * 3)
        value = byte2int(message, index + BYTE_SIZE * 3, index + BYTE_SIZE * 4)
        parameter = {
            'type': 'mq_integer',
            'struct_length': struct_length,
            'param_name': param_id,
            'value': value
        }
        params.append(parameter)
        return index + struct_length

    @staticmethod
    def _process_mq_string_param(message, index, params):
        struct_length = byte2int(message, index + BYTE_SIZE, index + BYTE_SIZE * 2)
        param_id = byte2int(message, index + BYTE_SIZE * 2, index + BYTE_SIZE * 3)
        encoding = byte2str(message, index + BYTE_SIZE * 3, index + BYTE_SIZE * 4)
        value_length = byte2int(message, index + BYTE_SIZE * 4, index + BYTE_SIZE * 5)
        value = byte2str(message, index + BYTE_SIZE * 5, index + BYTE_SIZE * 5 + value_length)
        parameter = {
            'type': 'mq_string',
            'struct_length': struct_length,
            'param_name': param_id,
            'encoding': encoding,
            'value_length': value_length,
            'value': value
        }
        params.append(parameter)
        return index + struct_length

    @staticmethod
    def _process_mq_string_list_param(message, index, params):
        struct_length = byte2int(message, index + BYTE_SIZE, index + BYTE_SIZE * 2)
        param_id = byte2int(message, index + BYTE_SIZE * 2, index + BYTE_SIZE * 3)
        encoding = byte2str(message, index + BYTE_SIZE * 3, index + BYTE_SIZE * 4)
        count = byte2int(message, index + BYTE_SIZE * 4, index + BYTE_SIZE * 5)
        value_length = byte2int(message, index + BYTE_SIZE * 5, index + BYTE_SIZE * 6)
        values = []
        for i in range(0, count):
            j = index + BYTE_SIZE * 6 + i * value_length
            values.append(byte2str(message, j, j + value_length))
        parameter = {
            'type': 'mq_string',
            'struct_length': struct_length,
            'param_name': param_id,
            'encoding': encoding,
            'count': count,
            'value_length': value_length,
            'values': values
        }
        params.append(parameter)
        return index + struct_length

    @staticmethod
    def _process_mq_integer_list_param(message, index, params):
        struct_length = byte2int(message, index + BYTE_SIZE, index + BYTE_SIZE * 2)
        param_id = byte2int(message, index + BYTE_SIZE * 2, index + BYTE_SIZE * 3)
        count = byte2int(message, index + BYTE_SIZE * 3, index + BYTE_SIZE * 4)
        values = []
        for i in range(0, count):
            j = index + BYTE_SIZE * (4 + i)
            values.append(byte2int(message, j, j + BYTE_SIZE))
        parameter = {
            'type': 'mq_string',
            'struct_length': struct_length,
            'param_name': param_id,
            'count': count,
            'values': values
        }
        params.append(parameter)
        return index + struct_length

    @staticmethod
    def _process_mq_string_byte_param(message, index, params):
        struct_length = byte2int(message, index + BYTE_SIZE, index + BYTE_SIZE * 2)
        param_id = byte2int(message, index + BYTE_SIZE * 2, index + BYTE_SIZE * 3)
        value_length = byte2int(message, index + BYTE_SIZE * 3, index + BYTE_SIZE * 4)
        value = byte2str(message, index + BYTE_SIZE * 4, index + BYTE_SIZE * 4 + value_length)
        parameter = {
            'type': 'mq_string_byte',
            'struct_length': struct_length,
            'param_name': param_id,
            'value_length': value_length,
            'value': value
        }
        params.append(parameter)
        return index + struct_length

    @staticmethod
    def _process_mq_group_param(message, index, params):
        struct_length = byte2int(message, index + BYTE_SIZE, index + BYTE_SIZE * 2)
        param_id = byte2int(message, index + BYTE_SIZE * 2, index + BYTE_SIZE * 3)
        param_count = byte2int(message, index + BYTE_SIZE * 3, index + BYTE_SIZE * 4)
        parameter = {
            'type': 'mq_group',
            'struct_length': struct_length,
            'param_name': param_id,
            'param_count': param_count,
        }
        params.append(parameter)
        return index + struct_length
