import ast
from threading import Thread

import pymqi

from mq_message import MQMessageParser

mq_properties = ast.literal_eval(open('resources/mq_properties.json').read())

#  move to docker env variables
Q_MGR_NAME = mq_properties['q_mgr_name']
CHANNEL = mq_properties['channel']
HOST = mq_properties['host']
PORT = mq_properties['port']
CONN_INFO = '%s(%s)' % (HOST, PORT)
WAIT_INTERVAL = mq_properties['wait_interval']
QUEUES = mq_properties['queues']


def monitor(queue_name):
    try:

        # Initialize MQ objects
        qmgr = pymqi.connect(Q_MGR_NAME, CHANNEL, CONN_INFO)
        queue = pymqi.Queue(qmgr, queue_name)

        md = pymqi.MD()
        gmo = pymqi.GMO()
        gmo.Options = pymqi.gmo.Options = pymqi.CMQC.MQGMO_WAIT | pymqi.CMQC.MQGMO_FAIL_IF_QUIESCING
        gmo.WaitInterval = WAIT_INTERVAL

        keep_running = True

        while keep_running:
            try:
                # Wait up to to gmo.WaitInterval for a new message.
                message = queue.get(None, md, gmo)
                parser = MQMessageParser()
                m = parser.parse(message, md)
                with open('logs/' + queue_name + '.log', 'a') as f:
                    f.write(m.to_json() + '\n')

                # Reset the MsgId, CorrelId & GroupId so that we can reuse the same 'md' object again.
                md.MsgId = pymqi.CMQC.MQMI_NONE
                md.CorrelId = pymqi.CMQC.MQCI_NONE
                md.GroupId = pymqi.CMQC.MQGI_NONE

            except pymqi.MQMIError, e:
                if e.comp == pymqi.CMQC.MQCC_FAILED and e.reason == pymqi.CMQC.MQRC_NO_MSG_AVAILABLE:
                    # No messages, that's OK, we can ignore it.
                    pass
                else:
                    # Some other error condition.
                    raise

        queue.close()
        qmgr.disconnect()

    except pymqi.MQMIError, e:
        print 'ERROR : [%s]' % e


if __name__ == "__main__":
    for q in QUEUES:
        Thread(target=monitor, args=(q,)).start()
